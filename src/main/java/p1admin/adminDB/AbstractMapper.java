package p1admin.adminDB;

import javax.sql.DataSource;
import java.sql.ResultSet;

/**
 * Created by Fran Lozano and Jose Maldonado on 05/03/2016.
 */
public abstract class AbstractMapper<T,K> {

    protected DataAccessor da;

    public AbstractMapper(DataSource ds) {
        this.da = new DataAccessor (ds);
    }

    public void insert (T object) {
        String tableName = getTableName();
        String [] tableColumns = getTableColumns();
        Object [] values  = getValues(object) ;
        Object id = da.insertRow(tableName,tableColumns,values);
        setID(object, id);
    }

    public void update (T object) {
        String tableName = getTableName();
        String [] keyColumns = getKeyColumns();
        Object [] keyValues = getKeyValues(object);
        String [] tableColumns = getTableColumns();
        Object [] values  = getValues (object) ;
        da.updateRows(tableName, keyColumns, keyValues, tableColumns, values);
    }

    public void delete (T object) {
        String tableName = getTableName();
        String [] keyColumns = getKeyColumns();
        Object [] keyValues  = getKeyValues(object) ;
        da.deleteRows(tableName,keyColumns,keyValues);
    }

    protected abstract String getTableName();

    protected abstract String[] getTableColumns();

    protected abstract Object[] getValues(T object);

    protected abstract String[] getKeyColumns();

    protected abstract Object[] getKeyValues(T object);

    protected abstract void setID(T object, Object id);

    protected abstract T buildObject(ResultSet rs);


}
