package p1admin.adminDB;

import p1admin.model.Opcion;
import p1admin.model.Pregunta;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Fran Lozano and Jose Maldonado on 05/03/2016.
 */
public class PreguntaMapper extends AbstractMapper<Pregunta,Integer> {


    public PreguntaMapper(DataSource ds) {
        super(ds);
    }

    @Override
    protected String getTableName() {
        return "preguntas";
    }

    @Override
    protected String[] getTableColumns() {
        String [] columnNames = new String[1];
        columnNames[0] = "enunciado";
        return columnNames;
    }

    @Override
    protected Object[] getValues(Pregunta object) {
        Object [] values = new Object[1];
        values[0] = object.getEnunciado();
        return values;
    }

    @Override
    protected String[] getKeyColumns() {
        String [] columnNames = new String[1];
        columnNames[0] = "idpregunta";
        return columnNames;
    }

    @Override
    protected Object[] getKeyValues(Pregunta object) {
        Object [] keyValues = new Object[1];
        keyValues[0] = object.getId();
        return keyValues;
    }

    @Override
    protected void setID(Pregunta object, Object key) {
        int id =  Integer.parseInt(key.toString());
        object.setId(id);
    }

    @Override
    protected Pregunta buildObject(ResultSet rs) {
        Pregunta p = new Pregunta();
        Integer id = null;
        String enunciado = null;
        try {
            id = rs.getInt(1);
            enunciado = rs.getString(2);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        p.setId(id);
        p.setEnunciado(enunciado);
        return p;
    }

    public List<Pregunta> getAll() {

        String tableNamePreguntas = "preguntas";
        String tableNameOpciones = "opciones";


        //SELECT * FROM preguntas t1 LEFT JOIN opciones t2 ON t1.idpregunta = t2.idpregunta ORDER BY t1.idpregunta DESC;

        /*Solution to n+1 queries problem*/
        String sql  = "SELECT * FROM " + tableNamePreguntas + " t1" +
                " LEFT JOIN " + tableNameOpciones + " t2" +
                " ON t1.idpregunta = t2.idpregunta" +
                " ORDER BY t1.idpregunta DESC";
        return getPreguntas(sql,null);
    }

    public List<Pregunta> getQuestionsContaining(String text) {

        String tableNamePreguntas = "preguntas";
        String tableNameOpciones = "opciones";

        /*Solution to n+1 queries problem*/
        String sql  = "SELECT * FROM " + tableNamePreguntas + " t1" +
                " LEFT JOIN " + tableNameOpciones + " t2" +
                " ON t1.idpregunta = t2.idpregunta" +
                " WHERE LOWER(enunciado) LIKE LOWER(?)" +
                " ORDER BY t1.idpregunta DESC";
        return getPreguntas(sql,text);
    }

    private List<Pregunta> getPreguntas(String sql, String text) {
        ResultSet rs = null;
        Pregunta p;
        int idPregunta = -1;
        List<Pregunta> preguntas = new ArrayList<>();

        try (Connection con = da.getDataSource().getConnection();
            PreparedStatement pst = con.prepareStatement(sql)){
            if(text != null)
                pst.setObject(1, "%"+text+"%");
            rs = pst.executeQuery();
            if (!rs.isClosed()) {
                while (rs.next()) {
                    if (idPregunta != rs.getInt(1)) {
                        /*New question*/
                        p = new Pregunta();
                        idPregunta = rs.getInt(1);
                        p.setId(idPregunta);
                        p.setEnunciado(rs.getString(2));
                        preguntas.add(p);
                    }
                    else {
                        p = preguntas.get(preguntas.size() - 1);
                    }
                    int id = 0;
                    int numeroOrden = 0;
                    String respuesta = null;
                    try {
                        id = rs.getInt(3);
                        numeroOrden = rs.getInt(4);
                        respuesta = rs.getString(6);
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }

                    if (!(id == 0  && numeroOrden == 0 && respuesta == null)){
                        Opcion o = new Opcion();
                        o.setIdRespuesta(id);
                        o.setTexto(respuesta);
                        o.setPreguntaMadre(p);
                        o.setNumeroOrden(numeroOrden);
                        p.addOpcion(o);
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return preguntas;
    }
}
