package p1admin.model;

import java.util.Date;

/**
 * Created by Fran Lozano and Jos� Maldonado on 03/03/2016.
 */
public class Invitacion extends Mensaje {
    private Pregunta pregunta;

    public Invitacion(int idMensaje, Date timestamp, boolean leido, Usuario origen, Usuario destino, Pregunta pregunta) {
        super(idMensaje, timestamp, leido, origen, destino);
        this.pregunta = pregunta;
    }

    public Pregunta getPregunta() {
        return pregunta;
    }

    public void setPregunta(Pregunta pregunta) {
        this.pregunta = pregunta;
    }
}
