package p1admin.model;

import java.util.Date;

/**
 * Created by Fran Lozano and Jos� Maldonado on 03/03/2016.
 */
public class Mensaje {

    private int idMensaje;
    private Date timestamp;
    private boolean leido;
    private Usuario origen;
    private Usuario destino;


    public Mensaje(int idMensaje, Date timestamp, boolean leido, Usuario origen, Usuario destino) {
        this.idMensaje = idMensaje;
        this.timestamp = timestamp;
        this.leido = leido;
        this.origen = origen;
        this.destino = destino;
    }

    public Usuario getOrigen() {
        return origen;
    }

    public void setOrigen(Usuario origen) {
        this.origen = origen;
    }

    public Usuario getDestino() {
        return destino;
    }

    public void setDestino(Usuario destino) {
        this.destino = destino;
    }

    public int getIdMensaje() {
        return idMensaje;
    }

    public void setIdMensaje(int idMensaje) {
        this.idMensaje = idMensaje;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public boolean isLeido() {
        return leido;
    }

    public void setLeido(boolean leido) {
        this.leido = leido;
    }
}

