package p1admin.model;

import java.util.Date;

/**
 * Created by Fran Lozano and Jos� Maldonado on 03/03/2016.
 */
public class Solicitud extends Mensaje {

    private String texto;
    private boolean aceptado;

    public Solicitud(int idMensaje, Date timestamp, boolean leido, Usuario origen, Usuario destino, String texto) {
        super(idMensaje, timestamp, leido, origen, destino);
        this.texto = texto;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }
}
