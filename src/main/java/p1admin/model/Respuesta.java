package p1admin.model;

/**
 * Created by Fran Lozano and Jos� Maldonado on 03/03/2016.
 */
public class Respuesta {

    private Opcion opcion;
    private int relevancia;

    public Respuesta(Opcion opcion, int relevancia) {
        this.opcion = opcion;
        this.relevancia = relevancia;
    }

    public Opcion getOpcion() {
        return opcion;
    }

    public void setOpcion(Opcion opcion) {
        this.opcion = opcion;
    }

    public int getRelevancia() {
        return relevancia;
    }

    public void setRelevancia(int relevancia) {
        this.relevancia = relevancia;
    }
}
