package p1admin.adminDB;

import javax.sql.DataSource;
import java.sql.*;
import java.util.Arrays;

/**
 * Created by Fran Lozano and Jose Maldonado on 05/03/2016.
 */
public class DataAccessor {

    private DataSource ds;

    public DataAccessor(DataSource ds) {
        this.ds = ds;
    }

    public Object insertRow (String tableName, String[] cols, Object[] values) {
        String sql = generateInsertStatement(tableName,cols);
        try(Connection con = ds.getConnection();
            PreparedStatement pst = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
            for (int i = 0; i < values.length; i++) {
                try {
                    pst.setObject(i + 1, values[i]);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
                pst.executeUpdate();
                ResultSet rs = pst.getGeneratedKeys();
                if(rs.next()) {
                    return rs.getObject(1);
                }
            } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean updateRows (String tableName , String[] keys, Object[] keysVals,String[] cols,Object[] newVals) {

        String sql = getUpdateStatement(tableName, keys, cols);
        try(Connection con = ds.getConnection();
            PreparedStatement pst = con.prepareStatement(sql)) {
            for (int i = 0; i < newVals.length; i++) {
                try {
                    pst.setObject( i + 1, newVals[i]);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

            for (int i = 0; i < keysVals.length; i++) {
                try {
                    pst.setObject(newVals.length + i + 1, keysVals[i]);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            int numRows = pst.executeUpdate();
            return (numRows >= 1);
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean deleteRows (String tableName, String[] keys, Object[] keysVals) {
        String sql = getDeleteStatement(tableName, keys);
        try(Connection con = ds.getConnection();
            PreparedStatement pst = con.prepareStatement(sql)) {
            for (int i = 0; i < keysVals.length; i++) {
                try {
                    pst.setObject(i + 1, keysVals[i]);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            int numRows = pst.executeUpdate();
            return (numRows >= 1);
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    private String generateInsertStatement(String tableName, String[] fields) {
        String fieldList = String.join(",", fields);
        String[] marks = new String[fields.length];
        Arrays.fill(marks, "?");
        String markList = String.join(",",marks);
        return "INSERT INTO " + tableName
                + " (" + fieldList + ") VALUES ("
                + markList + ")";
    }

    private String getUpdateStatement(String tableName, String[] keys, String[] cols) {
        StringBuilder sb = new StringBuilder();
        sb.append("UPDATE " + tableName);

        if (cols != null) {
            sb.append(" SET ");
            String columnsMarks = String.join("=?,", cols);
            sb.append(columnsMarks);
            sb.append("=?");
        }

        if(keys != null) {
            sb.append(" WHERE ");
            String keysMarks = String.join("=? AND ", keys);
            sb.append(keysMarks);
            sb.append("=?");
        }
        return sb.toString();
    }

    private String getDeleteStatement(String tableName, String[] cols) {
        StringBuilder sb = new StringBuilder();
        sb.append("DELETE FROM " + tableName);
        if (cols != null) {
            sb.append(" WHERE (");
            String columnsMarks = String.join("=? AND ", cols);
            sb.append(columnsMarks);
            sb.append("=?)");
        }
        return sb.toString();
    }

    public DataSource getDataSource() {
        return this.ds;
    }
}
