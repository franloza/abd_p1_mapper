package p1admin.adminDB;

import p1admin.model.Opcion;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Fran Lozano and Jose Maldonado on 05/03/2016.
 */
public class OpcionMapper extends AbstractMapper<Opcion,Integer> {

    public OpcionMapper(DataSource ds) {
        super(ds);
    }

    @Override
    protected String getTableName() {
        return "opciones";
    }

    @Override
    protected String[] getTableColumns() {
        String [] columnNames = new String[3];
        columnNames[0] = "numeroOrden";
        columnNames[1] = "idpregunta";
        columnNames[2] = "respuesta";
        return columnNames;
    }

    @Override
    protected Object[] getValues(Opcion object) {
        Object [] values = new Object[3];
        values[0] = object.getNumeroOrden();
        values[1] = object.getPreguntaMadre().getId();
        values[2] = object.getTexto();
        return values;
    }

    @Override
    protected String[] getKeyColumns() {
        String [] columnNames = new String[1];;
        columnNames[0] = "idrespuesta";
        return columnNames;
    }

    @Override
    protected Object[] getKeyValues(Opcion object) {
        Object [] keyValues = new Object[1];
        keyValues[0] = object.getIdRespuesta();
        return keyValues;
    }

    @Override
    protected void setID(Opcion object, Object key) {
        int id =  Integer.parseInt(key.toString());
        object.setIdRespuesta(id);
    }

    @Override
    protected Opcion buildObject(ResultSet rs) {
        Opcion o = new Opcion();
        Integer id = 0,numeroOrden = 0;
        String respuesta = null;
        try {
            id = rs.getInt(1);
            numeroOrden = rs.getInt(2);
            respuesta = rs.getString(4);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        o.setIdRespuesta(id);
        o.setTexto(respuesta);
        o.setNumeroOrden(numeroOrden);
        o.setPreguntaMadre(null);
        return o;
    }
}


